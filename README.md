# Sistemas Electrónicos para Automatización

Prácticas de Sistemas Electrónicos para Automatización del Grado en Ingeniería
Electrónica, Robótica y Mecatrónica de la Universidad de Sevilla. En este
repositorio se encontrarán únicamente las prácticas correspondientes a la parte
"FPGAs para Automatización" de la asignatura.


## Contenidos

[TOC]

# Software que utilizaremos

Se plantea esta parte de la asignatura de forma que se pueda realizar
utilizando únicamente herramientas libres o de código abierto.

## Síntesis y simulación

- [Yosys](https://github.com/YosysHQ/yosys) para síntesis
- [ghdlsynth](https://github.com/ghdl/ghdl-yosys-plugin) como front-end VHDL para yosys
- [GHDL](https://github.com/ghdl/ghdl) para simulación
- [gtkwave](https://github.com/gtkwave/gtkwave) para visualización de formas de onda
- [nextpnr](https://github.com/YosysHQ/nextpnr) para realizar el Place & Route de los diseños
- [icestorm](https://github.com/YosysHQ/icestorm) para generar los bitstreams y configurar la FPGA

## Compilación de código C

- [riscv-gnu-toolchain](https://github.com/riscv-collab/riscv-gnu-toolchain) para compilar software para el procesador empotrado

## Conexión a dispositivos UART

- [cutecom](https://gitlab.com/cutecom/cutecom) para conectarnos a la UART del microprocesador
- ~~[minicom](https://linux.die.net/man/1/minicom) para conectarnos a la UART del microprocesador~~
  - (Con minicom, por alguna razón, al enviar programas de más de 4kB al procesador, no se descargan correctamente)

## Instalación de las herramientas

**IMPORTANTE:** El alumno debe instalar las herramientas en su ordenador
portátil antes de la primera práctica. En caso de tener algún problema o duda,
debe escribir a los profesores de prácticas.

1. Descargar la máquina virtual de Ubuntu 22.04 del siguiente enlace y seguir las
   instrucciones:
   [http://trajano.us.es/~fjfj/mv/maq_virtual.html](http://trajano.us.es/~fjfj/mv/maq_virtual.html).
   No es necesario seguir las instrucciones de la sección "Fase 4. Instalación
   de software adicional"

   Esta máquina virtual es una copia de la distribución de linux existente en
   el Centro de Cálculo. Si ya la tenéis en vuestro ordenador, podéis omitir
   este primer paso.

   Se debe utilizar el usuario ``salas``, cuyo password es también ``salas``.
   Este usuario tiene permisos para hacer ``sudo``.

   _**Nota:** Si ya tienen una máquina virtual de Ubuntu 18.04 por otra asignatura, pueden crear un usuario llamado 'salas' en ella (o un enlace simbólico como se indica en https://gitlab.com/sepagierm/sepa-practicas/-/blob/main/faq/README.md#y-si-mi-usuario-no-se-llama-salas) y evitarse así el instalar una segunda máquina virtual_


2. Instalar gtkwave y cutecom, además de algunas dependencias de yosys y
   nextpnr, ejecutando los siguientes comandos en un terminal:

        sudo apt update
        sudo apt install gtkwave cutecom gnat zlib1g-dev libcanberra-gtk-module libboost-all-dev libftdi1

3. Descargar el software para la asignatura. Se proporciona un fichero con el
   software de síntesis e implementación ya compilado:

        cd ~
        wget https://heimdall.us.es/sw/fosshdl-sepa2023-ubuntu22.tar.gz

   Se puede comprobar que la descarga ha sido correcta utilizando el siguiente
   comando:

        md5sum fosshdl-sepa2023-ubuntu22.tar.gz 

   La salida de dicho comando debe incluir este código (si el código es
   distinto, significa que ha habido un error en la descarga):

        41c837c4e25d892c3bc12bab33315663  fosshdl-sepa2023-ubuntu22.tar.gz

   A continuación, descomprimir (usando tar) el fichero descargado:

        tar xzvf fosshdl-sepa2023-ubuntu22.tar.gz

   _**Nota:** Si están usando una máquina virtual de Ubuntu 18.04, deben descargar el fichero https://heimdall.us.es/sw/fosshdl-20211203-ubuntu18.04.tar.gz, cuyo md5sum es 1290686d0d7b95233334c33daf3d3dc6, y deben pasarle el nombre de dicho fichero a la herramienta tar para descompirmirlo: ``tar xzvf fosshdl-20211203-ubuntu18.04.tar.gz``_

4. Descargar el compilador para RISC-V. En este caso, descargamos los binarios
   ya compilados por el creador del procesador neorv32:

        cd ~
        wget https://github.com/stnolting/riscv-gcc-prebuilt/releases/download/rv32i-4.0.0/riscv32-unknown-elf.gcc-12.1.0.tar.gz
        mkdir riscv
        tar xzvf riscv32-unknown-elf.gcc-12.1.0.tar.gz -C riscv/

5. Añadir al fichero de variables de entorno la localización de los binarios:

        echo export PATH=\$PATH:/home/salas/riscv/bin >> ~/fosshdl/env.rc

6. Permitir el acceso a los dispositivos usb al usuario ``salas``:

        echo 'SUBSYSTEM=="usb", MODE="0660", GROUP="plugdev"' | sudo tee /etc/udev/rules.d/00-usb-permissions.rules
        sudo udevadm control --reload-rules

7. Añadir al usuario ``salas`` al grupo ``dialout`` para que pueda utilizar
   el terminal serie:

        sudo usermod -a -G dialout salas

   _**Nota:** Si el usuario del alumno no se llama ``salas``, debe cambiar ``salas`` por el nombre de su usuario en el comando anterior_

   Para que se actualicen los permisos tendremos que reiniciar la sesión así que llegados a este punto podemos directamente reiniciar la máquina virtual.

   Otra opción es usar el comando ``newgrp`` permite actualizar los permisos sin tener que salir y
   volver a entrar de la sesión, pero esto sólo nos funcionará en el shell en el que ejecutemos dicho comando:

        newgrp dialout
   
8. En la consola en la que vayamos a utilizar el software, cargar las variables
   de entorno:

        source ~/fosshdl/env.rc

_**Nota**:_ Si el alumno ha instalado las herramientas en una carpeta diferente o su usuario no se llama ``salas``, debe consultar las preguntas frecuentes [¿Y si mi usuario no se llama ``salas``?](faq/README.md#y-si-mi-usuario-no-se-llama-salas) e [¿Y si quiero instalar el software en una carpeta diferente?](faq/README.md#y-si-quiero-instalar-el-software-en-una-carpeta-diferente).

## Editores de texto

Se puede utilizar cualquier editor de texto para editar los ficheros con los
que trabajaremos en las prácticas (principalmente ficheros VHDL, C y
Makefiles). Se recomienda utilizar un editor de texto con el que se esté
familiarizado o, en caso de querer aprender a utilizar un editor nuevo, lo cual
siempre es una aventura interesante, no realizar dicho aprendizaje justo
durante las horas de prácticas :wink:. En cualquier caso, se dejan aquí algunas
sugerencias:

### TerosHDL

Para la asignatura os puede ser interesante utilizar la extensión
[TerosHDL](https://github.com/TerosTechnology/terosHDL) del editor Visual
Studio Code. Esta extensión, cuyos desarrolladores principales son antiguos
alumnos de la Escuela Técnica Superior de Ingeniería de la Universidad de
Sevilla, es software libre y ofrece muchas funcionalidades integradas para
facilitar el desarrollo con VHDL y Verilog.

#### Instalación de TerosHDL

- El primer paso es instalar Visual Studio Code: https://code.visualstudio.com/Download
- El segundo paso es instalar las librerías que necesita TerosHDL:

        sudo apt install python3-pip
        pip3 install teroshdl

- El tercer paso es abrir Visual Studio Code, ir a 'Extensions', buscar
  TerosHDL y hacer click en 'Install'
- Para hacer una primera toma de contacto con el editor, pueden seguir esta [práctica de introducción a TerosHDL](https://gitlab.com/edcmit/edc-practicas/-/tree/main/practica1) que he preparado para otra asignatura, en la que están desglosados los pasos para crear un proyecto y lanzar simulaciones.
- También es muy recomendable leer la [documentación completa de
  TerosHDL](https://terostechnology.github.io/terosHDLdoc/)

### Otros editores

#### gedit

Quien esté cómodo editando ficheros de texto plano en un editor gráfico
sencillo, puede usar ``gedit``:

        sudo apt install gedit

#### vim o emacs

Quien ya sepa manejar editores más complejos como ``vim`` o ``emacs`` puede
utilizarlos también. En particular, existen distribuciones de emacs como [Doom
emacs](https://github.com/hlissner/doom-emacs) o
[Spacemacs](https://www.spacemacs.org/) que ya incluyen paquetes para trabajar
con VHDL.

# Planificación de las prácticas

Esta parte de la asignatura comprende las siguientes sesiones prácticas:

1. [Manejo de las herramientas y toma de contacto con el procesador neorv32](practica1/)
2. [Control de un teclado numérico utilizando GPIO](practica2/)
3. [Creación de periféricos personalizados en VHDL](practica3/)
4. [Trabajo libre y ampliaciones](practica4/)
5. Presentaciones de los system-on-chip desarrollados (instrucciones en la [Enseñanza Virtual](https://ev.us.es))


# Preguntas frecuentes

Este repositorio contiene un documento de [preguntas frecuentes](faq/README.md)
el cual se irá actualizando durante el curso.

# Agradecimientos / Acknowledgement

Thank you to all developers and maintainers of the open source tools for
FPGA design and verification: Claire Wolf, Tristan Gingold, Myrtle Shah, Unai
Martínez-Corral, Matt Venn and many, many others...

Thank you also to Stephan Nolting, who developed the neorv32 processor we are
using here, for developing the processor and releasing it under a free software
license, and for answering our questions when preparing these lab sessions.
Further thanks to the RISC-V foundation for providing the RISC-V ISA under open
source licenses.

Without all your work, we would be stuck teaching System-on-Chip concepts with
proprietary ISAs, microprocessors and implementation software.

Finally, thanks to [Francisco José Fernández
Jiménez](http://trajano.us.es/~fjfj) for providing and maintaining, year after
year, a Virtual Machine that mimics the operating system in ETSI's computer
rooms. Your work has saved us a lot of time all these years.
