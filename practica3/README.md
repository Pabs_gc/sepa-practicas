# Práctica 3

Creación de periféricos personalizados en VHDL.

## Contenidos

[TOC]

# Objetivos

El objetivo técnico de esta práctica es conectar un periférico Wishbone descrito en VHDL al neorv32, y poder comunicarnos con él desde el software del microprocesador, consiguiendo de esta forma el poder integrar desarrollo software con desarrollo hardware.

Con esto se pretenden alcanzar los siguientes objetivos formativos:

- Incrementar la comprensión del funcionamiento del protocolo Wishbone en su modo clásico
- Aprender a conectar periféricos al interfaz Wishbone del procesador
- Aprender a comunicarnos por software con periféricos Wishbone, tanto en lectura como en escritura
- Incrementar la soltura con el neorv32 y las herramientas de implementación de hardware y compilación de software asociadas
- Contribuir al entendimiento del system-on-chip completo, en particular la división entre hardware y software y la comunicación entre ambos
- Dar al alumno las herramientas necesarias para que pueda desarrollar sus propias ampliaciones sobre el system-on-chip

# Trabajo previo

El alumno debe asegurarse de haber entendido la práctica anterior. En particular, deberían tener claro cuándo tienen que reimplementar el procesador (síntesis + place & route + generación bitstream) y cuándo es suficiente con recompilar el software (riscv-gcc).

También deben repasar y tener a mano el tema de teoría *"Tema 12.2: Creación de un periférico Wishbone"*.

Si no recuerdan el VHDL para síntesis, deben repasarlo. Tienen documentación de repaso disponible en la EV de la asignatura.

Finalmente, al igual que en el resto de prácticas, deberían echar un vistazo a la documentación que se indica en la
siguiente sección, [Documentación importante](#documentación-importante).

# Documentación importante

## Documentación de las librerías software del neorv32

En particular, para poder comunicarse con los periféricos conectados al bus Wishbone, deben leer la
documentación correspondiente a las funciones que sirven para acceder a posiciones específicas de memoria en lectura y escritura. Pueden encontrar estas funciones en la sección ``neorv_32_cpu`` de la documentación de las librerías software:

> - La documentación de las librerías software del procesador está en
>   https://hipolitoguzman.github.io/neorv32/sw/files.html

# Trabajo a realizar

El alumno debe instanciar el periférico Wishbone que se proporciona en este mismo repositorio, en [practica3/wb_regs.vhd](practica3/wb_regs.vhd), y demostrar comunicación efectiva con el mismo desde el software. Es muy recomendable leer el código del periférico para entender su funcionamiento.

Para instanciar el periférico y poder utilizarlo, se debe:

1. Crear las señales necesarias para el bus Wishbone en el top-level del diseño. Se recomienda utilizar una forma de nombrar las señales que no de pie a confusión, como se comentó en clase. Una opción es indicar, en el nombre de la señal, si es 'slave-to-master' (``s2m``) o 'master-to-slave' (``m2s``)
2. Conectar dichas señales al interfaz Wishbone del neorv32
   - _Nota:_ La versión del neorv32 que estamos utilizando señaliza erróneamente errores de timeout si el generic ``MEM_EXT_TIMEOUT`` toma justo un valor que sea potencia de 2. Para esta práctica podemos simplemente poner dicho generic a ``0``, de forma que si existe un error en la conexión veremos que el procesador se queda colgado y sabremos que es un timeout del bus. Si los periféricos están bien desarrollados, no debería haber timeouts. De todas formas, si en algún momento lo necesitamos, podemos asignar cualquier valor para ``MEM_EXT_TIMEOUT`` positivo que no sea potencia de 2 (por ejemplo, ``255``)
3. Asegurarse de que el generic ``MEM_EXT_EN`` del neorv32 toma el valor ``true``, ya que si no, no se instanciará el intefaz Wishbone
4. Instanciar el periférico Wishbone, conectándolo a esas mismas señales (teniendo en cuenta que las señales escritas por el maestro son leídas por el esclavo y viceversa). En la instancia, se debe indicar una dirección base y un tamaño de memoria para el periférico de entre las que estén disponibles en el sistema
5. Modificar el fichero ``setups/osflow/filesets.mk`` para que se incluya el fichero ``.vhd`` en el que está descrito el periférico
6. Volver a realizar la implementación del hardware (síntesis + place & route + generación bitstream), corrigiendo cualquier error que les indiquen las herramientas
7. Modificar el software para poder acceder a las direcciones de memoria del periférico
8. Probar el sistema y demostrar comunicación, tanto en escritura como en lectura, con todos los registros del periférico

Una vez el periférico esté funcionando correctamente, el alumno puede plantear las ampliaciones que prefiera, valorándose más las ampliaciones que realicen modificaciones a nivel hardware que las que sean puramente implementadas en software. Algunas ampliaciones que se pueden hacer a partir de este momento son: 

1. Realizar una calculadora sencilla. Pueden recibir los operandos 1 y 2 por dos direcciones diferentes, el operador (suma, resta, multiplicación, etc) por una tercera dirección, e indicar en una cuarta dirección (que será leída desde el software) el resultado del cálculo. Si necesitan más de un ciclo de reloj para realizar los cálculos, tendrán que tenerlo en cuenta a la hora de activar la señal ``wb_ack_o``.
2. Añadir más registros al periférico. El periférico tiene 4 registros que son accesibles desde el mapa de memoria del microprocesador, pero el alumno podría, una vez comprobado su correcto funcionamiento, modificarlo para que tuviera más registros si los necesitara.
3. Reimplementar el control del teclado numérico en un periférico Wishbone. El periférico devolverá, a través de un valor almacenado en una dirección de memoria que será leída por el procesador, la tecla pulsada o incluso un código que indique las teclas pulsadas en caso de detectarse varias pulsaciones (esto se puede conseguir por ejemplo con 16 bits, activando un bit por cada pulsación detectada). Si realizan esta ampliación, no sobreescriban el código de la práctica 2, ya que dicho código también tendrán que entregarlo y presentarlo.
4. Uso de memorias de bloque de un puerto y de doble puerto. Adicionalmente a lo comentado en el punto 2, si el alumno quisiera utilizar memorias de bloque de la FPGA en lugar de registros, puede fijarse en el código proporcionado por el desarrollador del neorv32 en https://gist.github.com/stnolting/51bbdd92d4b1166a0af49d01b5c061ef. Dicho código al sintetizarse debe inferir una memoria de bloque (salvo tal vez en casos en los que se instancien memorias muy pequeñas). También sería posible modificar dicho código para que infiriera una memoria de doble puerto, de forma que nuestro código VHDL acceda a los datos por un puerto dedicado e independiente del puerto que se usa para recibir y enviar datos al bus Wishbone. Esto tiene sentido cuando necesitamos que el procesador comparta grandes cantidades de datos con nuestro periférico. De esta forma, se puede realizar cualquier código VHDL que queramos, y mientras cumpla con el protocolo Wishbone, el microprocesador podrá comunicarse con él ya que podrá ver sus registros o memorias como palabras dentro del mapa de memoria del procesador. Se incluyen esta práctica códigos VHDL que infieren una [memoria de bloque](practica3/bram.vhd) y una [memoria de bloque de doble puerto](practica3/dpram.vhd) al sintetizarse. Se podría modificar el código proporcionado por el desarrollador del neorv32 para que instanciara una memoria de doble puerto.

Finalmente, el alumno es libre de realizar sus propias ampliaciones
en función de sus inquietudes.



# Preparación para la siguiente práctica

La siguiente práctica es [Trabajo libre y ampliaciones](practica4). Pueden ir leyendo el enunciado de la misma e ir mirando la documentación de aquello sobre lo que quieran realizar la ampliación. Por supuesto también pueden ir implementando y probando hardware y software para dichas ampliaciones si quieren.

Finalmente, y a riesgo de insistir demasiado:

> Si están usando ``git``, no olviden 'commitear' y 'pushear' sus cambios. Si no
> lo están usando, hagan al menos una copia de seguridad de su trabajo.

