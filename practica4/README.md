# Práctica 4

Trabajo libre y ampliaciones.

## Contenidos

[TOC]

# Objetivos

El objetivo técnico de esta práctica es abierto: esta práctica está pensada para dejarles espacio y tiempo para poder realizar las ampliaciones que deseen sobre el sistema.

Con esto se pretenden alcanzar los siguientes objetivos formativos:

- Incrementar la comprensión del system-on-chip completo, a partir de experiencias con aspectos concretos del mismo
- Incrementar la confianza del alumno en sus capacidades para realizar desarrollos utilizando esta tecnología
- Emular un entorno real en el que deben buscar información por su cuenta, de distintas fuentes y autores, y aplicarla a un desarrollo técnico
- Potenciar el aprendizaje de por vida (_lifelong learning_), de gran importancia en el Espacio Europeo de Educación Superior

# Trabajo previo

El alumno debe asegurarse de haber entendido la práctica anterior.

Si no recuerdan el VHDL para síntesis, deben repasarlo. Tienen documentación de repaso disponible en la EV de la asignatura.

Finalmente, al igual que en el resto de prácticas, deberían echar un vistazo a la documentación que se indica en la
siguiente sección, [Documentación importante](#documentación-importante).

# Documentación importante

En esta práctica, la documentación a consultar depende de vosotros: en función de lo que quieran hacer tendrán que consultar una documentación u otra. No obstante, se les indica aquí dónde encontrar la documentación de los PMOD extras que se les han proporcionado con el kit de la FPGA para prácticas.

## Datasheet del PMOD CON3

El PMOD CON3 sirve para conectar servomotores a la tarjeta iCEBreaker a través de un conector PMOD. Se puede consultar la documentación del PMOD en: https://digilent.com/reference/pmod/pmodcon3/start

Se proporciona un servo de rotación continua (360º) con el kit que se les ha entregado. Pueden encontrar la información del mismo aquí: https://es.farnell.com/multicomp-pro/mp-708-0002/servomotor-360/dp/3359812, pero son libres de utilizar otros servos de los que puedan disponer, si son compatibles con el PMOD.

## Datasheet del PMOD LS1

El PMOD CON3 sirve para conectar sensores ópticos a la tarjeta iCEBreaker a través de un conector PMOD. Se puede consultar la documentación del PMOD en: https://digilent.com/reference/pmod/pmodls1/start

Se proporciona un sensor de infrarrojos con el kit que se les ha entregado. Pueden encontrar la información del mismo aquí: https://digilent.com/shop/ir-proximity-sensor-2-pack/, pero son libres de utilizar otros sensores de los que puedan disponer, si son compatibles con el PMOD.

## Documentación vista en las anteriores prácticas

No se olviden del Datasheet y User Guide del neorv32, así como de la documentación de las librerías software del procesador. Se recuerda que en la [práctica1](practica1/README.md) tienen disponibles los enlaces a dicha documentación.

## Otro hardware disponible

Tenemos algunos módulos hardware de los cuales no tenemos suficientes para entregar uno a cada pareja de alumnos. Estos módulos se prestarán a parejas de alumnos que hayan realizado avances significativos y por orden de solicitud, siempre teniendo en cuenta que posiblemente tengan que compartirlos con los compañeros.

- PMOD JSTK2: Joystick con botón central y led programable: https://digilent.com/reference/pmod/pmodjstk2/start
- PMOD VGA: Proporciona un puerto VGA con 12 bits por cada componente de color: https://digilent.com/reference/pmod/pmodvga/start 
- PMOD BB: Breadboard con conector PMOD: https://digilent.com/reference/pmod/pmodbb/start
- PMOD AD1: Convertidor analógico-digital: https://digilent.com/reference/pmod/pmodad1/start
- PMOD DA2: Convertidor digital-analógico: https://digilent.com/reference/pmod/pmodda2/start
- PMOD RTCC: reloj en tiempo real y calendario: https://digilent.com/reference/pmod/pmodrtcc/start
- PMOD WiFi: transceptor WiFi (802.11g): https://digilent.com/reference/pmod/pmodwifi/start
- PMOD 7 segmentos: https://github.com/icebreaker-fpga/icebreaker-pmod
- PMOD DIP switches: https://github.com/icebreaker-fpga/icebreaker-pmod
- PMOD de la colección 'Analog Essentials Collection', que incluye, entre otros, convertidores analógico-digital y digital-analógico, y un potenciómetro digital: https://www.maximintegrated.com/en/design/technical-documents/brochures/5/5468.html
- Cables de extensión PMOD de 6 y 12 pines: https://es.rs-online.com/web/p/accesorios-para-kits-de-desarrollo/1346437

# Trabajo a realizar

El alumno debe aprovechar esta sesión para profundizar más en algún aspecto que le haya llamado la atención del microprocesador.

Algunos ejemplos de ampliaciones que se pueden hacer son:

1. Control de servomotores
2. Manejo del sensor de infrarrojos
3. Aprender a manejar las interrupciones del neorv32
4. Buscar un interconnect Wishbone que sea libre o de código abierto, e integrarlo en el system-on-chip, demostrando su funcionamiento con al menos dos periféricos Wishbone (pueden ser dos instancias diferentes del mismo)
5. Aprender a manejar otros periféricos internos del neorv32, como pueden ser el generador de números aleatorios, el interfaz SPI, el timer, o el controlador PWM
6. Integrar distintos componentes hardware, utilizando el software como control global del sistema (por ejemplo, pueden tomar datos del sensor de infrarrojos y, en función de dichos datos, mover el servo)
7. Realizar un hardware a medida y conectarlo a un conector PMOD (con muchísimo cuidado de no cortocircuitar nada)
8. Si trabajan con su propio periférico Wishbone a medida, pueden realizar un driver para el mismo. Es decir, crear funciones de alto nivel para que el usuario del periférico no tenga que leer y escribir en registros, sino utilizar directamente funciones más complejas, en cuya implementación sí se trabaja directamente con los registros. De esta forma, el usuario se abstrae del mapa de registros del periférico y no necesita conocerlo al detalle. Puntos extra si generan la documentación del driver usando Doxygen (u otro generador de documentación)
9. Cualquier otra ampliación que se les ocurra, aunque no esté contemplada en estos ejemplos

Se deja total libertad al alumnado para realizar las ampliaciones que más les motiven. Se valorará la complejidad de las mismas. Al igual que en el resto de prácticas, se valora más el desarrollo (o integración) de hardware, y el aprender a manejar elementos nuevos.

# Preparación para la siguiente práctica

En la última sesión práctica se realizarán las defensas de los system-on-chip desarrollados. Se les dejará en la EV instrucciones para la entrega.



