# Práctica 2

Control de un teclado numérico utilizando GPIO.

## Contenidos

[TOC]

# Objetivos

El objetivo técnico de esta práctica es conectar un teclado PMOD a la FPGA
y controlarlo utilizando el GPIO.

Con esto se pretenden alcanzar los siguientes objetivos formativos:

- Aprender a realizar pequeñas modificaciones sobre el diseño hardware
- Consolidar el manejo del GPIO por software
- Trabajar con datasheets y documentación de terceros de distintos orígenes
- Realizar el control por software de un hardware externo
- Incrementar la soltura con el neorv32 y las herramientas de implementación de hardware y compilación de software asociadas

# Trabajo previo

El alumno debe asegurarse de haber entendido la práctica anterior.

También harían bien en repasar los [operadores a nivel de bits en
C](https://en.wikipedia.org/wiki/Bitwise_operations_in_C), tales como ``&``, ``|``,
``>>``, ``<<``, ``~``, etc, ya que pueden ser importantes para que el código quede
sencillo. Se recuerda que, ante la duda, pueden practicar estos operadores sin
necesidad de tener el hardware delante: pueden utilizar un compilador de C,
hacer pruebas e imprimir el resultado por pantalla. Incluso pueden utilizar un
compilador online como http://codepad.org/ , y tardarán pocos minutos en
realizar el repaso.

Finalmente, deberían echar un buen vistazo a la documentación que se indica en la
siguiente sección, [Documentación importante](#documentación-importante), lo
que les ahorrará tiempo durante la sesión práctica, permitiéndoles enfocarse
más en las modificaciones hardware y software.


# Documentación importante

Para esta práctica necesitarán principalmente consultar cuatro documentos, de
los cuales tres se mencionaron en el enunciado de la práctica anterior:

## Documentación del teclado PMOD

La documentación del periférico, el teclado PMOD, se encuentra en el siguente
enlace: https://www.digilentinc.com/Products/Detail.cfm?Prod=PMODKYPD (una vez
estén en la web, hagan click en "Support").

Se recomienda leer atentamente la descripción, prestando especial atención a la
polaridad de las señales.

También se recomienda que estudien el esquemático del
circuito. Cuando miren el esquemático, les recomiendo que piensen cómo sería
el proceso completo para realizar la lectura de una pulsación concreta.

## Documentación de la tarjeta iCEBreaker

Dependiendo de a qué conector PMOD vayan a conectar el teclado, tendrán que
conectar los pines del GPIO a unos u otros pines de la FPGA. La información de
qué pines de la FPGA están conectados a qué pines de qué conector PMOD puede
encontrarse en el repositorio de la tarjeta iCEBreaker:

> Cuando trabajamos con FPGAs y tarjetas de desarrollo, siempre es buena práctica
> el tener a mano los enlaces a los datasheets de las mismas. Podemos encontrar
> toda la información relativa a la tarjeta iCEBreaker aquí:
> https://github.com/icebreaker-fpga/icebreaker

## Fichero de pines que estamos utilizando

En la práctica anterior se comentó que sería interesante que miraran este fichero.
Para esta práctica, será indispensable haberlo consultado:

> - El fichero iCEBreaker.pcf con las restricciones de implementación, el cual
>   incluye la localización de todos los pines de usuario de la FPGA en la
>   tarjeta iCEBreaker, que está en ``setups/osflow/constraints/iCEBreaker.pcf``.
>   Cuando necesitemos añadir pines externos a nuestro sistema, querremos saber qué
>   hay conectado, en la PCB, a los pines de usuario de la FPGA.

## Documentación de las librerías software del neorv32

En particular, para poder manejar correctamente el GPIO, deben leer la
documentación correspondiente a las funciones que sirven para acceder al GPIO en lectura y escritura:

> - La documentación de las librerías software del procesador está en
>   https://hipolitoguzman.github.io/neorv32/sw/files.html

# Trabajo a realizar

El alumno debe utilizar el GPIO del sistema para controlar el teclado PMOD
Keypad de Digilent. Este es un pequeño teclado numérico hexadecimal que se
conecta a los puertos de expansión PMOD de la tarjeta iCEBreaker. PMOD es un interfaz
especificado por Digilent que está disponible en muchas tarjetas de lógica
programable.

El sistema modificado debe detectar la tecla pulsada y escribir el carácter
recibido por la UART.

Para ello, el primer paso será estudiar el fichero top-level que estamos
implementando (se recuerda que está en
``neorv32/setups/osflow/board_tops/neorv32_iCEBreaker_BoardTop_MinimalBoot.vhd``)
y comprobar qué pines del gpio están conectado a los pines externos. En caso de
que sea necesario, tendremos que conectar los pines que necesitemos.

Seguramente tengamos que conectar los leds que estuvimos moviendo en la
práctica anterior a pines distintos del GPIO, o directamente desconectarlos.

Un par de ampliaciones sencillas que se pueden realizar sobre esta práctica son:

1. Implementar alguna clase de mecanismo para que una misma pulsación no repita
   indefinidamente el cáracter recibido por la UART.
2. Conectar los LEDs de la práctica anterior a otros pines del GPIO y poder controlar
   desde el mismo programa tanto los LEDs como el teclado numérico. Dependiendo de las funciones específicas de la librería del GPIO del neorv32 que hayan utilizado, puede ser que para conseguir esto necesiten realizar un uso elegante de los operadores a nivel de bits en C,

Por supuesto el alumno es libre de realizar sus propias ampliaciones
en función de sus inquietudes.

_Nota_: La conexión física entre los puertos PMOD de la iCEBreaker y el Keypad,
tanto si se realiza directamente entre conectores como a través de un cable de
extensión, debe conectar cada pin de un conector al pin del mismo número del
otro (es decir, el 1 al 1, el 2 al 2, etc…). Si estamos conectando el PMOD de
la forma correcta, los pines de GND del PMOD quedarán conectados a los pines de
GND de la iCEBreaker, y los pines de VCC del PMOD quedarán conectados a los
pines de VCC de la iCEBreaker.

# Preparación para la siguiente práctica

Lean todo lo que puedan sobre el bus Wishbone y cómo se utiliza, ya que en la
siguiente práctica tendrán que implementar un periférico que se conecte al bus
Wishbone del neorv32.

También deben repasar el VHDL, ya que es el lenguaje en el que realizarán las modificaciones al periférico que implementaremos en la siguiente práctica

Si están usando ``git``, no olviden 'commitear' y 'pushear' sus cambios. Si no
lo están usando, hagan al menos una copia de seguridad de su trabajo.
