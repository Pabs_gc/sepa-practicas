# Práctica 1

Manejo de las herramientas y toma de contacto con el procesador neorv32.

## Contenidos

[TOC]

# Objetivos

Los objetivos de esta sesión práctica son los siguientes:

  - Realizar una toma de contacto con el microprocesador neorv32
  - Afianzar los conceptos vistos en las clases de teoría
  - Realizar una implementación básica del procesador neorv32, incluyendo hardware y software
  - Comprobar que el microprocesador funciona correctamente
  - Aprender a descargar programas en el microprocesador usando el bootloader
  - Aprender a realizar pequeñas modificaciones sobre el software

# Trabajo previo

El alumno debe asegurarse de que ha seguido las instrucciones en [Instalación
de las
herramientas](https://gitlab.com/sepagierm/sepa-practicas#instalaci%C3%B3n-de-las-herramientas)

Es recomendable comprobar que las herramientas están instaladas y se encuentran
en el $PATH, ejecutando uno por uno los siguientes comandos:

        source ~/fosshdl/env.rc
        ghdl --version
        gtkwave --version
        yosys --version
        nextpnr-ice40 --version
        icepack --version
        iceprog --help

# Documentación importante

## Datasheet de la tarjeta de desarrollo iCEBreaker

Cuando trabajamos con FPGAs y tarjetas de desarrollo, siempre es buena práctica
el tener a mano los enlaces a los datasheets de las mismas. Podemos encontrar
toda la información relativa a la tarjeta iCEBreaker aquí:
https://github.com/icebreaker-fpga/icebreaker

Un primer vistazo a la documentación revela que la tarjeta dispone, entre otras cosas, de una FPGA
Lattice iCE40UP5k, 16 MB de memoria flash, un chip FTDI para comunicaciones a
través del puerto USB, y 39 pines para entrada y salida, de los cuales los más
importantes para la asignatura son los 3 puertos PMOD (Peripheral Module).

## Datasheet de la FPGA utilizada

La FPGA utilizada es una Lattice iCE40 UltraPlus 5K, en encapsulado QFN de 48
pines. Podemos encontar el datasheet de la familia en este enlace:
http://www.latticesemi.com/-/media/LatticeSemi/Documents/DataSheets/iCE/iCE40-UltraPlus-Family-Data-Sheet.ashx

En ese datasheet podremos consultar cómo es la arquitectura de las FPGAs, qué
clase de componentes internos tiene (por ejemplo, cuántas entradas tiene cada
LUT), la capacidad de las memorias internas, etc.

## Datasheet del procesador y guía de usuario

El procesador neorv32 está muy documentado, de forma que el alumno puede
profundizar todo lo que quiera en la arquitectura, periféricos, funcionamiento,
etc, lo que será útil a la hora de realizar ampliaciones sobre las prácticas.

_Nota:_ Debido a que el procesador está en constante desarrollo y mejora, para la asignatura hemos hecho un fork del proyecto, de forma que la versión del mismo, sus interfaces hardware y software y su documentación se mantengan estables durante todo el cuatrimestre. La documentación de la versión que vamos a utilizar este curso se encuentra en los siguientes enlaces:

- El Datasheet del procesador neorv32 está en
  https://hipolitoguzman.github.io/neorv32
- El User Guide del procesador neorv32 está en
  https://hipolitoguzman.github.io/neorv32/ug


## Documentación de las librerías software

Las funciones software que proporciona el microprocesador para acceder a sus distintas funcionalidades están documentadas en el siguiente enlace:

- La documentación de las librerías software del procesador está en
  https://hipolitoguzman.github.io/neorv32/sw/files.html

_Nota:_ Si se fijan, la documentación está generada con [Doxygen](https://www.doxygen.nl), el generador de documentación que vimos el curso pasado en _Proyectos Integrados_.

# Algunas nociones sobre ``make`` y los ficheros ``Makefile``

- Las instrucciones para implementación del VHDL y compilación del hardware en
  este repositorio están descritas usando ficheros ``Makefile``, que se
  utilizan invocando a la herramienta ``make``. Un fichero Makefile está
  compuesto por reglas (_rules_). Dichas reglas lo que hacen es simplemente relacionar un
  fichero objeto (el _objetivo_ o _target_) con los ficheros fuente a partir de
  los que se genera (sus _dependencias_ o _dependencies_), indicando asimismo el
  comando que es necesario ejectuar para crear dicho objetivo a partir de sus
  dependencias (la _receta_ o _recipe_).

  De esta forma, un fichero ``Makefile`` está compuesto principalmente por
  reglas. La sintaxis básica de una regla en un ``Makefile`` es muy sencilla,
  como se ve a continuación (es importante que la línea de la receta empiece con un
  tabulador, si no, ``make`` dará un error):

	  target: lista_de_dependencias
        receta para construir el target

  Un ejemplo podría ser una regla para compilar un fichero c:

	  main.out: main.c
        gcc -g -Wall main.c -o main.out

  Usando esta sintaxis, podemos escribir reglas para cualquier cosa que se
  pueda construir utilizando la línea de comandos, por ejemplo:

  Analizar ficheros vhdl:

	  fichero.o: fichero.vhd
        ghdl -a fichero.vhd

  Convertir imagen vectorial (svg) a pdf:

	  imagen.pdf: imagen.svg
        inkscape -D -z --file=imagen.svg --export-pdf=imagen.pdf --export-latex

  Compilar un .tex, generando un .pdf usando latex:
  
      fichero.pdf: fichero.tex imagen.pdf
          latexmk -pdf -pdflatex="pdflatex -interaction=nonstopmode" -use-make fichero.tex

  La complejidad aquí radica en el hecho de que tenemos que conocer dos cosas: la
  sintaxis de un ``Makefile`` y cómo se usa por línea de comandos el programa que
  quieras utilizar. Lo normal es leer la documentación del programa, probar
  primero en el shell y cuando ya tengas el comando claro, añadirlo a tu ``Makefile``.

  Para generar un _target_ específico, se invoca a ``make`` pasándole como argumento el objetivo
  específico o incluso varios objetivos separados por espacio, por ejemplo: ``make main.out imagen.pdf``.
  Si se invoca a ``make`` sin pasarle ningún argumento, construirá únicamente el primer _target_
  que aparezca en el ``Makefile``. Es costumbre (aunque no obligación) que el nombre de dicho
  primer _target_ sea ``all``. Asimismo, un _target_ puede depender de otros _targets_, por lo que
  durante una invocación a ``make`` pueden utilizarse múltiples reglas y de esta forma generar
  múltiples ficheros. Por ejemplo, si el _target_ ``all`` de un ``Makefile`` que contuviera también
  las reglas anteriores fuera el siguiente:

      all: main.out fichero.o imagen.pdf fichero.pdf

  Al ejecutar ``make`` se generarían los cuatro ficheros indicados. Esta regla es interesante,
  ya que no contiene ninguna receta. No es necesario que contenga ninguna ya que para crear
  cada uno de los cuatro ficheros utilizará las reglas que ya están en el ``Makefile``.

  De esta forma, no es necesario memorizar todos los comandos para generar el
  build de un proyecto, ya sea sencillo o complejo, y además esto posibilita
  automatizar los pasos, lo que se vió que era una buena práctica de desarrollo
  durante el curso pasado, en la asignatura _Proyectos Integrados_.


## Makefile nivel avanzado

  A veces los Makefiles se complican, y si no entendemos lo que está pasando
  puede parecernos innecesariamente complejo. Sin embargo, la realidad es más
  sencilla. En general, un ``Makefile`` puede complicarse de tres maneras:

  1. Porque en la receta (es decir, en el comando a ejecutar) se invoque
     directamente a ``make``. Esto normalmente se hace especificando, con -C, el
     directorio en el que está ese ``Makefile`` que se va a ejecutar. De esta forma, es
     como si hiciera "cd" a ese directorio y luego ejecutara "make":

          path/a/target/que/necesito:
            make -C ruta/donde/esta/el/Makefile

     o, particularizando:

          otracarpeta/main.out:
            make -C otracarpeta

     esto es equivalente a decirle al shell "vete a la carpeta especifica y
     allí ejecuta ``make``", y tiene sentido cuando ya tienes un Makefile que
     hace lo que quieres hacer (de esta forma lo reutilizas sin tener que
     escribirlo de nuevo)

  2. Porque en el fichero ``Makefile`` se haga un ``include`` de otro fichero,
     normalmente con extensión ``.mk``. Por ejemplo:

	      include ../common.mk

     Esto se usa cuando tienes reglas que vas a usar en varios sitios. Por
     ejemplo, si tenemos una regla genérica para generar un ``.o`` en función de un
     ``.vhd``, la escribiríamos en ese fichero common.mk, el cual incluiremos desde
     el resto de los Makefiles, y así sólo la tendremos que escribir una vez. La
     sintaxis de un fichero ``.mk`` es exactamente la misma que la de un
     ``Makefile``. Es muy típico el tener un fichero ``common.mk`` en la carpeta
     raíz de un proyecto, y tener en ese fichero reglas útiles para el proyecto
     además de ciertas constantes o configuraciones (como nombres de las
     herramientas a utilizar, localización de las mismas, flags específicos para
     compilación, etc)

  3. Porque haga uso de reglas genéricas. Por ejemplo, si queremos compilar
     varios ficheros C, podríamos tener una regla para cada uno:

	      main.out: main.c
		      gcc -g -Wall main.c -o main.out

	      main2.out: main2.c
		      gcc -g -Wall main2.c -o main2.out

	      otro.out: otro.c
		      gcc -g -Wall otro.c -o otro.out
	
	      otro2.out: otro2.c
		      gcc -g -Wall otro2.c -o otro2.out
	
     Sin embargo, aquí se puede ver un patrón que se repite. Para generar el _target_,
     siempre se compila la primera _dependencia_, pasando el flag ``-o`` (_output_)
     al compilador con el nombre del _target_. GNU Make permite generalizar esta
     regla de la siguiente manera:

	      %.out: %.c
		      gcc -g -Wall $< -o $@

     De esta forma, cuando ``make`` tenga que generar un fichero ``.out``,
     mirará si existe un fichero ``.c`` con el mismo nombre (salvo la extensión), y
     aplicará el comando indicado. ¿Qué son esos símbolos que aparecen en la regla?
     Son símbolos para  ``make``:

     - ``%`` es el comodín (o _wildcard_). Es parecido a cuando usamos un ``*`` en
       un shell, sólo que ``%`` debe 'matchear' con una cadena no vacía (es decir, que
       tenga al menos un carácter)
     - ``%.out`` es un patrón (_pattern_), que corresponde al fichero que queremos generar, por ejemplo ``main.out`` 'matchearía' con el patrón ``%.out``



     Los símbolos que aparecen en la receta son las llamadas _variables
     automáticas_ de ``make``, y algunos ejemplos de estas son:

     - ``$@`` es el _target_ u objetivo, es decir el fichero que quieres generar
     - ``$<`` es la primera depedencia
     - ``$^`` es la lista de dependencias, separadas por espacio (por ejemplo: "``main.c main.h``")

     Se puede encontrar más información en
     [la sección "Pattern Rules" de la documentación de GNU Make](https://www.gnu.org/software/make/manual/html_node/Pattern-Rules.html),
     en particular la lista completa de variables automáticas se encuentra en
     [la subsección "Automatic Variables"](https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html).

# Ficheros que el alumno debería mirar

- La mayoría de ficheros ``Makefile`` del neorv32 indican cómo deben usarse, listando los _targets_ disponibles, si
  llamamos a la herramienta ``make`` sin pasarle argumentos. De esta forma, será
  sencillo saber cómo se compilan o construyen distintos elementos software o
  hardware, simplemente invocando a ``make`` en las carpetas apropiadas. Después,
  podemos llamar a ``make`` pasándole como argumento el _target_ específico que
  querríamos construir, aunque será recomendable añadirle también la opción ``-n``
  la primera vez, para que indique qué comandos va a ejecutar.
- En particular es interesante el Makefile que sirve para hacer la implementación
  utilizando las herramientas libres para diseño FPGA, situado en
  ``setups/osflow/Makefile``. Este Makefile se llama a sí mismo con diferentes
  targets e incluye a otros Makefiles, así que será importante echarle un
  vistazo para poder seguirle la pista. Se recomienda echarle un vistazo e invocar
  a ``make`` con la opción -n para ver los comandos que se ejecutarán.
- El fichero iCEBreaker.pcf con las restricciones de implementación, el cual
  incluye la localización de todos los pines de usuario de la FPGA en la
  tarjeta iCEBreaker, que está en ``setups/osflow/constraints/iCEBreaker.pcf``.
  Cuando necesitemos añadir pines externos a nuestro sistema, querremos saber qué
  hay conectado, en la PCB, a los pines de usuario de la FPGA.

# Primeros pasos con neorv32

## Clonar el repositorio del microprocesador

Lo primero que tenemos que hacer para poder empezar a trabajar con el
microprocesador es clonar el repositorio del mismo.

Si bien el repositorio original del procesador neorv32 está en
https://github.com/stnolting/neorv32 , no obstante nosotros vamos a clonar el
fork que hemos preparado para la asignatura en
https://gitlab.com/sepagierm/neorv32 , que incluye el soporte para la tarjeta
iCEBreaker (en un branch aparte, ``icebreaker``)

    cd ~
    git clone https://gitlab.com/sepagierm/neorv32 --branch icebreaker



## Configuración e implementación del microprocesador

Los distintos top-level para implementación utilizando herramientas libres se encuentran en ``neorv32/setups/osflow/board_tops`` y
son realmente ficheros top-level descritos en VHDL.

Podemos generar nuestra propia configuración modificando el top-level o incluso
añadiendo un top-level nuevo, aunque para esta práctica podemos utilizar el
top-level tal y como se nos proporciona.

Implementaremos, para la tarjeta iCEBreaker, el top-level MinimalBoot. Este
top-level instancia el micro en una configuración en la que contiene un
bootloader. De esta forma, se le podrán descargar programas a través del puerto
serie sin necesidad de volver a reimplementar el VHDL. Se recomienda leer el
código del fichero .vhd correspondiente (en este caso,
``neorv32_iCEBreaker_BoardTop_MinimalBoot.vhd``)

Primero, invocamos ``make`` sin ningún argumento para ver cómo debe usarse el
Makefile:

    cd ~/neorv32/setups/osflow
    make

Se puede ver que es necesario definir la variable BOARD y especificar un target
para el Makefile que será el board top. En nuestro caso, es claro que queremos
la tarjeta iCEBreaker y el top-level MinimalBoot:

    make BOARD=iCEBreaker MinimalBoot

## Compilación del software que ejecutará el microprocesador

Vamos a compilar un ejemplo software. Para ello entramos en la carpeta del
ejemplo ``blink_led`` e invocamos a ``make``:

    cd ~/neorv32/sw/example/blink_led
    make

Vemos que, al igual que el resto de makefiles del neorv32, al invocar a
``make``sin más argumentos, nos indica los targets disponibles. Como queremos
compilar un ejecutable para descargar utilizando el bootloader, generaremos el
target ``exe``:

    make exe

Si ahora listamos los ficheros en el directorio, debemos ver que existe un
fichero llamado ``neorv32_exe.bin``:

    ls

## Configuración de la FPGA

Una vez generado el bitstream de la FPGA, puede configurarse la misma usando la
herramienta ``iceprog``, pasándole como argumento el bitstream generado, por
ejemplo:

    cd ~/neorv32/setups/osflow
    iceprog neorv32_iCEBreaker_MinimalBoot.bit

Por supuesto, debemos haber conectado la tarjeta a nuestro ordenador y haber
asignado el dispositivo USB a la máquina virtual.

Para asignar el dispositivo USB a la máquina virtual, los programas de
virtualización VirtualBox y VMware no conectan automáticamente todos los
dispositivos USB a todas las máquinas virtuales, sino que el usuario debe
indicar cuál quiere conectar. Esto se hace en VirtualBox en el menú "Devices"
-> "USB", y seleccionando el dispositivo que se quiere conectar a la máquina
virtual, mientras que para hacerlo en VMware tenemos que ir al menú "VM" ->
"Removable Devices" -> hacer click en el nombre del dispositivo y luego en
"Connect". En nuestro caso, el dispositivo que queremos conectar es el "FTDI
Dual RS232-HS".

**IMPORTANTE:** no se debe apoyar la tarjeta en (ni colocarla cerca de) nada
metálico, como las bisagras de las mesas del Centro de Cálculo, las anillas de
un cuaderno de anillas, sacapuntas u otro material que pueda ser conductor de
la electricidad. No queremos cortocircuitar nodos aleatoriamente dentro de la
tarjeta. Ante la duda, es buena práctica colocar la tarjeta encima de su caja,
si ésta es de cartón o plástico.

Si ``iceprog`` detecta la tarjeta y puede configurarla correctamente, en la consola
veremos unos mensajes del tipo:

    init..
    cdone: high
    reset..
    cdone: low
    flash ID: 0xEF 0x70 0x18 0x00
    file size: 104090
    erase 64kB sector at 0x000000..
    erase 64kB sector at 0x010000..
    programming..
    reading..
    VERIFY OK
    cdone: high
    Bye.

Por el contrario, si iceprog no detecta la tarjeta, veremos en la consola un
mensaje de error del tipo:

    Can't find iCE FTDI USB device (vendor_id 0x0403, device_id 0x6010 or 0x6014).
    ABORT.

Si no estamos seguros de si la máquina virtual está viendo la tarjeta, podemos
listar los dispositivos USB:

    lsusb

Y deberíamos ver, entre los dispositivos listados, un dispositivo con Vendor ID 0403 y Product ID 6010, por
ejemplo:

    Bus 002 Device 010: ID 0403:6010 Future Technology Devices International, Ltd FT2232C/D/H Dual UART/FIFO IC

En caso de no tenerlo, debemos asegurarnos de que tenemos el USB conectado
físicamente y asignado el dispositivo USB a la máquina virtual.

En caso de que el dispositivo aparezca en la salida de ``lsusb`` pero no
podamos configurar la FPGA, debemos comprobar que hemos permitido el acceso a
los dispositivos usb al usuario ``salas`` como se indica en las instrucciones
en [Instalación de las
herramientas](../README.md#instalaci%C3%B3n-de-las-herramientas), en particular
debemos comprobar que el fichero de reglas para el dispositivo es correcto:

    cat /etc/udev/rules.d/00-usb-permissions.rules 
    SUBSYSTEM=="usb", MODE="0660", GROUP="plugdev"

Si no, debemos seguir los pasos indicados. Ya que estas reglas se aplican
cuando se conecta el dispositivo a la máquina virtual, si ya lo tenemos
conectado, debemos desasignarlo y volverlo a asignar a la máquina virtual.

## Comunicarnos con el microprocesador

Una vez configurada la FPGA, podremos comunicarnos por el microprocesador a
través de la UART.

Para ello, abriremos ``cutecom``, desde el menú Aplicaciones -> Herramientas del sistema, o bien pulsando <kbd>Alt+F2</kbd>, escribiendo ``cutecom`` y pulsando en ``Ejecutar``, o directamente ejecutando ``cutecom`` en otra consola.

Una vez abierto el programa, debemos configurar el puerto serie. Para ello, pulsaremos en ``Settings`` y configuraremos la UART a 19200 baudios, 8 bits de datos, sin paridad y 1 bit de parada (lo que se conoce
como 8-N-1), y desactivaremos el control de flujo si estuviera activado (tanto el software como el hardware). En 'Open Mode' seleccionaremos 'Read/Write', ya que queremos tanto leer lo que nos envíe el procesador como poder enviarle datos y comandos.

Una vez configurado correctamente, seleccionamos como dispositivo serie ``/dev/ttyUSB1``, y pulsamos ``Open``.

También debemos asegurarnos que, a la derecha del campo ``Input``, esté seleccionada la opción ``None``, ya que si tenemos otra opción marcada (por ejemplo ``LF``) ``cutecom`` enviará caracteres extra (como 'retorno de carro' o 'nueva línea') junto con cada pulsación que enviemos, lo que confundirá al procesador.

_Nota:_ El chip FTDI tiene 2 canales, como puede verse en el [diagrama de bloques
de la tarjeta
iCEBreaker](https://github.com/icebreaker-fpga/icebreaker/blob/master/img/icebreaker-block-diagram.jpg).
Un canal está conectado a la memoria Flash en la que se almacena el
bitstream, y el otro canal está conectado directamente a los pines de la FPGA,
a los cuales está conectada la UART del microprocesador. Es por esta razón por
la que, al conectar la FPGA aparecen dos dispositivos serie, ``dev/ttyUSB0``
(conectado a la Flash) y ``/dev/ttyUSB1`` (conectado a la FPGA). El dispositivo 0
desaparecerá la primera vez que usemos ``iceprog`` y no debemos conectarlo a
cutecom.

## Uso del bootloader

Queremos utilizar el bootloader para cargar el ejecutable compilado
anteriormente. Para ello, una vez configurado correctamente cutecom, podemos
resetear el microprocesador pulsando el botón que está en la tarjeta, al lado
del puerto microUSB, marcado como 'UBUTTON'. Al hacer esto, veremos los
siguientes mensajes en cutecom:

	<< NEORV32 Bootloader >>                                                        
	                                                                                
	BLDV: Oct 17 2021                                                               
	HWV:  0x01060205                                                                
	CLK:  0x0112a880                                                                
	MISA: 0x40801105                                                                
	CPU:  0x00000041                                                                
	SOC:  0x0067000d                                                                
	IMEM: 0x00010000 bytes @0x00000000                                              
	DMEM: 0x00010000 bytes @0x80000000                                              
	                                                                                
	Autoboot in 8s. Press key to abort.      

Antes de que pasen los 8 segundos, deberemos enviar una pulsación de una tecla cualquiera para
que el bootloader nos muestre los comandos disponibles. Debido a cómo funciona cutecom, debemos teclear una letra en el campo ``Input`` y pulsar <kbd>Enter</kbd>, para que se envíe dicha letra (el enter no se envía, es simplemente para indicar a cutecom que debe enviar lo que hayamos tecleado en ``Input``).

	Autoboot in 8s. Press key to abort.                                             
	Aborted.                                                                        
                                                                                
	Available CMDs:                                                                 
	 h: Help                                                                        
	 r: Restart                                                                     
	 u: Upload                                                                      
	 s: Store to flash                                                              
	 l: Load from flash                                                             
	 e: Execute                                                                     
	CMD:> 

Como queremos subir un programa al bootloader, enviaremos una <kbd>u</kbd> para seleccionar el comando 'Upload':

	CMD:> u                                                                         
	Awaiting neorv32_exe.bin... 

En este momento, tenemos que indicarle a cutecom que envíe el fichero
ejecutable a través de la UART (no vamos a teclearlo a mano nosotros). Para
ello, haremos click en el botón ``Send file...``, y se abrirá una ventana en la que podemos seleccionar el fichero ``neorv32_exe`` que hemos compilado anteriormente. Una vez seleccionado, haremos click en ``Open`` y se enviará el fichero al bootloader.

Una vez transmitido el fichero, tenemos que usar el comando 'Execute' (<kbd>e</kbd>) para que
el microprocesador ejecute el programa que le hemos descargado:

	Awaiting neorv32_exe.bin... OK                                                  
	CMD:> e                                                                         
	Booting...                                                                      
	Blinking LED demo program
             
Si al resetear el microprocesador pasan los 8 segundos sin haber pulsado ninguna tecla, el microprocesador
intentará arrancar buscando un programa en la memoria flash externa.


# Manejo de LEDs y botones a través del GPIO

El objetivo de la práctica, además de hacer una primera toma de contacto con el
microprocesador, es realizar un control sencillo de los LEDs de la tarjeta.
Estos LEDs están conectados a un GPIO en el top_level que se proporciona para
la tarjeta iCEBreaker, en
``setups/osflow/board_tops/neorv32_iCEBreaker_BoardTop_MinimalBoot``. Se
recomienda revisar dicho top-level para saber cuántos pines del GPIO (y qué
pines específicamente) están conectados a los LEDs, y a qué LEDs, y estudiar la
documentación del microprocesador (en particular, la [documentación de las librerías software](https://hipolitoguzman.github.io/neorv32/sw/files.html)) para
saber cuáles son las funciones que permiten acceso al GPIO. Por supuesto, ya
que está disponible, también pueden leer el código del ejemplo ``blink_led``
que acaban de probar.

Deben realizar sus modificaciones software en una carpeta software nueva, por
ejemplo pueden copiar la carpeta ``blink_led`` y modificar a partir de ahí.
Deben asegurarse de que entienden cómo funciona el acceso al GPIO, ya que será
importante para la segunda práctica.

También pueden realizar las modificaciones hardware que quieran, ya que conocen
el lenguaje VHDL. Para esta práctica no les será necesario pero para la
siguiente sí, así que no olviden familiarizarse con el fichero top_level que
están implementando.

Finalmente, **no se olviden de guardar copia de seguridad de su trabajo**, a
ser posible en un repositorio ``git`` (pero cualquier opción vale más que no tener
ninguna copia).
Como los alumnos conocen el uso de la herramienta ``git`` ya que han tenido la
asignatura _Proyectos Integrados_ durante el curso anterior, es **muy
recomendable** realizar un 'fork' propio del repositorio y añadir los cambios en
dicho fork, para no perder código en caso de fallo de hardware, borrado
accidental, corrupción de ficheros de la máquina virtual u otros.
Se recuerda que gitlab permite hacer 'fork' de repositorios,
con lo que una buena opción puede ser hacer un fork de
https://gitlab.com/sepagierm/neorv32 . En caso de hacer un fork del
repositorio, se recomienda que trabajen en un _branch_ aparte del branch
_icebreaker_, ya que esto les ahorrará _merge conflicts_ si los profesores
tenemos que actualizar el código en la rama _icebreaker_. Eso si, el branch que
creen debe partir de _icebreaker_ y no de _master_, ya que los ficheros para la
tarjeta iCEBreaker están la rama que lleva el mismo nombre.

En el siguiente enlace pueden encontrar una introducción a ``git`` por línea de comandos: https://gitlab.com/hgpub/gitintro. También pueden refrescar el manejo de la GUI de ``git`` con las siguientes transparencias: https://heimdall.us.es/docs/docencia/buenaspracticas/Guia_rapida_de_Git_en_modo_grafico.pdf

En caso de no utilizar ``git`` para gestionar el código, se deben realizar copias
de seguridad periódicamente.

# Preparación para la siguiente práctica

Lean todo lo que puedan de la documentación disponible en [Datasheet del
procesador y guía de usuario](#datasheet-del-procesador-y-guía-de-usuario). Una
buena forma de encarar la lectura de documentación técnica es leerla
_jerárquicamente_, es decir, primero los índices, tomando nota mental de qué
cosas pueden ser interesantes (librerías software, funcionamiento del
bootloader, generics para configurar el microprocesador) y luego leer en
profundidad aquellas secciones que hayan encontrado interesantes.

