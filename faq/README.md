# Preguntas frecuentes

[TOC]

## Estoy cansado de teclar siempre los mismos comandos. ¿Hay algo que pueda hacer para escribir menos?

  Recuerda que, en la consola de comandos, puedes usar flecha arriba y flecha
  abajo para moverte por el histórico de comandos

  Adicionalmente, pulsando <kbd>Ctrl+r</kbd> puedes buscar hacia atrás un comando específico (reverse-search)

## ¿Cómo puedo borrar los ficheros de implementación del micro, para empezar una implementación limpia desde cero, o para entregar el código sin ficheros binarios?

  Buena pregunta. En el directorio setups/osflow, el comando ``make clean`` no
  funciona directamente, porque ese target no está definido en el Makefile sino
  en common.mk. Parece que la forma correcta de invocar el target clean es
  haciendo:

        make BOARD=iCEBreaker TASK=clean run

  Pero por alguna razón no llega a eliminar los ficheros, no obstante
  puedes copiar el comando rm -rf que aparece y ejecutarlo manualmente:

        rm -rf *.{asc,bit,cf,dfu,history,json,o,svf} *-report.txt

  - [ ] TODO: enterarse de por qué falla ese comando dentro del Makefile. Si
  añadimos un target clean en el Makefile principal poniendo el rm -rf, el log
  indica que se ejecuta el comando pero no se eliminan los ficheros. Hay que ver
  si es algo específico de la versión de ubuntu, de bash, de make o de otra cosa. Tiene pinta de que puede ser por la expansión del blob ``*.{asc,bit,...}``

## ¿Puedo usar otro terminal serie? No me gusta mucho cutecom

  Por supuesto, puedes usar el terminal serie que prefieras, pero tienes que tener cuidado ya que algunos
  limitan el tamaño del fichero máximo que se puede descargar a 4kB, por lo que no funcionarán programas
  más grandes. Hay más información en este issue del neorv32: https://github.com/stnolting/neorv32/pull/215
  
  Mi recomendación es que pruebes el programa que prefieras pero, si te da problemas, vuelvas a cutecom.

## Me quedo muy rápido sin memoria, ¿qué puedo hacer?

  Ten cuidado con las funciones que permiten imprimir con formato, estilo ``printf()``. Estas funciones generalmente consumen mucha memoria, por lo que es preferible utilizar funciones de print con capacidades reducidas (por ejemplo que no formateen números, o que sólo puedan dar formato a ciertos tipos de datos).

  Otra opción que tienes es incrementar el tamaño de la memoria. Para incrementar la memoria disponible en el microprocesador, debes hacer:

  1. Indicar al microprocesador que debe instanciarse con más memoria. Para ello, se debe modificar el valor del generic ``MEM_INT_DMEM_SIZE`` en el top-level del microprocesador en ``setups/osflow/board_tops/neorv32_iCEBreaker_BoardTop_MinimalBoot.bit``

  2. Indicar el compilador de C que ahora tiene más memoria disponible para los programas. Para ello, modificar el linker script en ``sw/common/neorv32.ld``, en particular la línea que indica

    ram  (rwx) : ORIGIN = 0x80000000, LENGTH = DEFINED(make_bootloader) ? 512 : 8*1024

Para que indique el nuevo tamaño de memoria disponible, por ejemplo si ``MEM_INT_DMEM_SIZE => 64*1024``, el linker script debe indicar:

    ram  (rwx) : ORIGIN = 0x80000000, LENGTH = DEFINED(make_bootloader) ? 512 : 64*1024
  
  - Esto no explica toda la historia completa. Si miras tanto el [datasheet de la FPGA](http://www.latticesemi.com/-/media/LatticeSemi/Documents/DataSheets/iCE/iCE40-UltraPlus-Family-Data-Sheet.ashx) como los ficheros ``setups/osflow/devices/ice40/neorv32_dmem.ice40up_spram.vhd`` y ``setups/osflow/devices/ice40/neorv32_dmem.ice40up_spram.vhd``, verás que tanto la memoria de instrucciones como la memoria de datos instancian 2 primitivas de memoria de las cuales sólo hay 4 en la FPGA. Y siempre instancian la misma cantidad de memoria (64 kB para cada memoria), independientemente del valor de los generics. En el branch ``icebreaker`` hemos asignado a ``MEM_INT_DMEM_SIZE`` el valor ``8*1024`` simplemente para que sea compatible con el valor que figura por defecto en el linker script del repositorio.


## ¿Puedo poner el microprocesador a más frecuencia?

  Claro! Sólo tienes que:

  1. Instanciar un PLL (Phase-Locked Loop) en el top-level para convertir el reloj de entrada de 12MHz en un reloj a la frecuencia que quieras. Debes conectar el nuevo reloj (la salida del PLL) al microprocesador y, para no mezclar dominios de reloj (lo que da muchísimos problemas a nivel de timing) debes usar sólo ese nuevo reloj en todo el diseño (es decir, el reloj de 12 MHz **únicamente** lo conectarás al PLL). Aquí tienes una plantilla para instanciar el PLL:

  Librería y package en el que está definido el PLL (deben incluirse en la sección library del top-level en el que instanciemos el PLL):

```vhdl
library iCE40;
use iCE40.components.all;
```

  Declaración del componente:
`
```vhdl
-- Component declaration  
-- Internal component (hardware primitive) of the ICE40 FPGA
component SB_PLL40_PAD is
  generic (
    FEEDBACK_PATH : string                       := "SIMPLE";
    DIVR          : std_logic_vector(3 downto 0) := "0000";
    DIVF          : std_logic_vector(6 downto 0) := "1000001";
    DIVQ          : std_logic_vector(2 downto 0) := "011";
    FILTER_RANGE  : std_logic_vector(2 downto 0) := "001"
    );
  port (
    PACKAGEPIN   : in  std_logic;
    PLLOUTGLOBAL : out std_logic;
    RESETB       : in  std_logic;
    BYPASS       : in  std_logic;
    LOCK         : out std_logic
    );
end component;
```

  Instanciación del componente. En los comentarios se explica cómo se calcula la frecuencia de salida en función de la frecuencia de entrada y los generics del componente, no obstante esta información también se puede obtener usando la utilidad ``icepll``, includa en icestorm.
  
```vhdl
-- Connect an internal PLL (Phase-Locked Loop) to the 12 MHz clk to generate
-- a 50 MHz clk we will use internally
--
-- DIVR : Reference clk divider (0 to 15)
-- DIVF : Feedback divider (0 to 63)
-- DIVQ : VCO divicer (1 to 6)
-- FILTER_RANGE : PLL filter range (0 to 7)
--
-- The iCE40 sysCLOCK PLL Design and Usage Guide explains how F_pllout is
-- calculated with respect to the DIV* parameters.
-- If FEEDBACK_PATH="SIMPLE":
--
--              F_referenceclk * (DIVF+1)
--   F_pllout = -------------------------
--              (2**DIVQ) * (DIVR+1)
--
-- In our case we want to generate a 50MHz clock from a 12MHz clock, so we 
-- want to multiply by 50/12 = 25/6:
--
--              F_reference_clk * (24+1)
--   F_pllout = ------------------------
--                  (2**1) * (2+1)
--
pll_inst : SB_PLL40_PAD
  generic map (
    FEEDBACK_PATH => "SIMPLE",
    DIVR          => std_logic_vector(to_unsigned(2,4)),
    DIVF          => std_logic_vector(to_unsigned(24,7)),
    DIVQ          => std_logic_vector(to_unsigned(1,3)),
    FILTER_RANGE  => "001"
    )
  port map (
    PACKAGEPIN   => clk12,
    PLLOUTGLOBAL => clk,
    RESETB       => '1',
    BYPASS       => '0',
    LOCK         => pll_lock
    );
```

  2. Indicar al microprocesador, en su generic ``CLOCK_FREQUENCY``, la frecuencia real del reloj nuevo que le estás poniendo. Si no lo haces, la UART no te hablará a 19200 baudios, sino a vete-a-saber cuántos baudios.

  3. Indicar al programa de place and route, nextpnr, que la frecuencia a la que debe funcionar el diseño es ahora mayor. Para ello, debes modificar ``setups/osflow/boards/index.mk`` para que, al rutar para la tarjeta iCEBreaker (donde dice ``ifeq($(BOARD),iCEBreaker)``), los flags de place & route (PNRFLAGS) incluyan la frecuencia que quieres. Por defecto verás que ``--freq`` vale 13, lo que significa que la frecuencia de reloj esperada es de 13 MHz (aunque el reloj externo va a 12, le puse un poco de margen por si acaso).

## Vale, ¿y qué frecuencia máxima aguanta el microprocesador?

  Buena pregunta! La respuesta es que dependerá de la FPGA. Para la iCE40, ¿por qué no lo mides? Intenta rutar el microprocesador con distintas restricciones de frecuencias (simplemente tienes que cambiar el parámetro ``--freq`` en el ``index.mk`` mencionado anteriormente). Una recomendación es hacer una búsqueda dicotómica, por ejemplo sabes que a 13 MHz no hay problema, puedes probar con una frecuencia alta (por ejemplo 100 o 150 MHz) y si falla, reduces a la mitad, pero si ruta el diseño sin error, incrementas un 50%.

## ¿Y si mi usuario no se llama ``salas``?

  Si ya tienes un usuario en la máquina virtual o la versión específica de ubuntu instalada de forma nativa en tu PC, debido a cómo funciona ``ghdl``, que busca una librería dinámica en una localización concreta, vamos a tener que crear un enlace simbólico, ``/home/salas``, que apunte a tu ``$(HOME)``. Si por ejemplo tu usuario se llama 'miusuario' debes hacer:

    sudo ln -s /home/miusuario /home/salas

## ¿Y si quiero instalar el software en una carpeta diferente?

  Si quieres instalar el software en una carpeta que no sea la carpeta por defecto, puedes hacerlo, pero debes modificar el fichero ``fosshdl/env.rc`` para que en el ``$(PATH)`` se añada la carpeta en la que hayas instalado realmente el software.

## ¿Alguna guía para usar TerosHDL?

  Para hacer una primera toma de contacto con el editor, pueden seguir esta [práctica de introducción a TerosHDL](https://gitlab.com/edcmit/edc-practicas/-/tree/main/practica1) que he preparado, en la que están desglosados los pasos para crear un proyecto y lanzar simulaciones.

## ¿Cómo puedo simular un periférico Wishbone? No me funciona mi código y no logro saber por qué

  Como bien dices, si has hecho cambios complejos en el código VHDL y no te funciona, necesitas simularlo para encontrar el origen del error.

  Hacer esto es relativamente sencillo si usas TerosHDL, por lo que el primer paso es seguir la práctica de introducción a TerosHDL que está enlazada en la [respuesta anterior](#Alguna-guía-para-usar-teroshdl).

  Una vez que tengas ese manejo con el editor, el siguiente paso es crear un proyecto de TerosHDL y añadir el código de tu periférico. Tras eso, puedes generar una plantilla para el testbench como se ha visto en la guía anterior. Debes añadir un mecanismo de asfixia de reloj para asegurar que la simulación termina, también como se ve en la guía enlazada. Para eliminar los errores que da TerosHDL de que no encuentra la librería neorv32, tenemos que añadir dicha librería al proyecto, creando un library (con el botón 'add library'), poniéndole de nombre ``neorv32`` y añadiendo a ese library el fichero ``neorv32/rtl/core/neorv32_package.vhd``. Igualmente necesitaríamos añadir otros ficheros adicionales del neorv32 si los estuviéramos usando en nuestro periférico, pero el ejemplo que se proporciona en la [práctica 3](practica3) sólo necesita el fichero indicado.
  
  Tras esto, necesitarás generar las formas de onda del protocolo wishbone desde tu proceso de estímulos para simular la comunicación con el microprocesador, y ya estarás listo para simular tu periférico Wishbone.

  - [ ] TODO : para el curso que viene, añadir una plantilla para esto con un par de procedures read/write

## ¿Dónde puedo repasar cómo manejarme con un repositorio ``git``?

  En el siguiente enlace puedes encontrar una introducción a git por línea de comandos: https://gitlab.com/hgpub/gitintro
